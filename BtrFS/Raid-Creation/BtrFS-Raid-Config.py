#!/usr/bin/python 

import sys
import os
import json
import subprocess
import time


#######################################################
# Global Vars / Config
#######################################################
# This defines the disk drives that should be on the system
# NOTE: you can use lsblk command to see what drives are on your system
raidDrives = (
    '/dev/vda', # 0 position is reserved for primary system drive
    '/dev/vdb',
    '/dev/vdc',
    '/dev/vdd',
    '/dev/vde',
    '/dev/vdf',
    '/dev/vdg',
    '/dev/vdh')

# Size settings for partitions
maxSizeEFI = 512              # 512 MB
maxSizeSwap = 1024            # 512-1024 MB per disk
maxSizeRoot = 3072            # 2048 MB per disk (3072 - 1024)
maxSizeHome = '100%'          # fill the rest of the disk

# Select RAID mode for each drive
raidToggle={
    'sysroot':'raid0',
    'home':'raid0'
}


# number of current partition
partitionNum = 0 

# Directory that gets created / mounted for building RAID
tempMountDir = '/btrfsraidmnt'

# Menu toggle for Root format only
rootOnly = False


#######################################################
# Safety test for each drive
#######################################################
def check_drive(path):
    # Checks to see if the drive exists (returns true/false)
    check = os.path.exists(path)
    if not check:
        print('Drive ' + path + ' does not exist, check its path and try again.')
        return False
    
    #######################################################
    
    # Checking if a drive has any mountpoints
    process = subprocess.run(
        [
            "/usr/bin/lsblk",       # Bash command for finding mountpoints
            "--json",               # output to JSON format
            "-o",                   # This is the parm for which columns to display
            "MOUNTPOINT",           # Only display the "MOUNTPOINT" column
            path                    # this is the drive that you want to check
        ],
        capture_output=True,
        text=True)

    partitions = json.loads(process.stdout)['blockdevices']

    for i in partitions:
        text = json.dumps(i['mountpoint'])
        if text != 'null':
            print('Drive ' + path + ' is mounted, must unmount before continuing.  Check this using lsblk command.')
            return False
    
    #######################################################
    
    # if all checks pass, return confirmation
    print('Drive ' + path + ' passed safety checks.')
    return True


#######################################################
# Create new partition on drive
#######################################################
def partition_flag(path, flag, toggle):
    # Parms:
    # path = Path to drive
    # partNum = Partition number to apply flag to
    # flag = Which flag to set
    # toggle = on/off
    
    global partitionNum
    
    rc = subprocess.run(
    [
        "/usr/bin/parted",      # Bash command for managing partitions
        "--script",             # script flag to remove confirmation / error prompts
        path,                   # this is the drive that you want to check
        "set",                  # Set the flag
        str(partitionNum),           # Partition number to apply flag to
        flag,                   # Which flag to set
        toggle                  # on/off
    ],
    capture_output=True)
    
    if rc.returncode != 0:
        print('Filed to set ' + flag + ' flag on drive ' + path + str(partitionNum) + ' - Return Code: ' + str(rc.returncode))
        print(rc)
        return False
    
    return True


#######################################################
# Create new partition on drive
#######################################################
def create_partition(path, name, start, end, type):
    # Parms:
    # path = Path to drive
    # name = name / label of drive
    # start = location on disk to start partition
    # end = location on disk to end partition
    # type = the partition type (btrfs, fat32, swap, etc)
    
    global partitionNum
    
    if not rootOnly:
        rc = subprocess.run(
        [
            "/usr/bin/parted",      # Bash command for managing partitions
            "--script",             # script flag to remove confirmation / error prompts
            path,                   # this is the drive that you want to check
            "mkpart",               # Create new partition
            name,                   # The name / label of the drive
            str(start),             # location on disk to start partition
            str(end)                # location on disk to end partition
        ],
        capture_output=True)

        if rc.returncode != 0:
            print('Filed to create ' + name + ' partition on drive ' + path + ' - Return Code: ' + str(rc.returncode))
            print(rc)
            return False
    
    partitionNum += 1
    
    #######################################################
    
    # sometimes this processes so fast that it the next 
    # command executes before the partition is created.
    time.sleep(1) 
    
    # translation switch for formatting
    newPartition = path + str(partitionNum)
    formatType={
        'fat32':["/usr/bin/mkfs.fat", "-F", "32", newPartition],
        'btrfs':["/usr/bin/mkfs.btrfs", "-f", newPartition],
        'swap':["/usr/bin/mkswap", "-f", newPartition],
        'ext4':["/usr/bin/mkfs.ext4", "-F", newPartition]
    }

    # Reformat the drive
    rc = subprocess.run(formatType[type], capture_output=True)
    
    if rc.returncode != 0:
        print('Filed to format partition ' + newPartition + ' with type ' + type + ' - Return Code: ' + str(rc.returncode))
        print(rc)
        return False

    #######################################################
    
    # Add flag for swap partition
    if type == 'swap':
        rc = partition_flag(path, 'swap', 'on')
                
        if rc:
            rc = os.system('swapon ' + newPartition)
            # print(rc)  # 0 = good
        else:
            # error message already handled by function
            return False

    # Partition created successfully
    return True


#######################################################
# Formats each drive
#######################################################
def format_drive(path):
    # Used global variables
    global raidDrives
    global maxSizeEFI
    global maxSizeSwap
    global maxSizeRoot
    global maxSizeHome
    
    # Size settings for partitions
    primaryDrive = raidDrives[0]
    
    #######################################################
    
    # wipe drive (wipefs --all --force <path>)
    if not rootOnly:
        rc = subprocess.run(
        [
            "/usr/bin/wipefs",      # Bash command for wiping the drive
            "--all",                # Wipes everything
            "--force",              # Makes the wipe happen without confirmation
            path                    # this is the drive that you want to wipe
        ],
        capture_output=True)
        
        if rc.returncode != 0:
            print('Filed to wipe drive ' + path + ' - Return Code: ' + str(rc.returncode))
            print(rc)
            return False
    
    #######################################################
    
    # Create new partition table (parted <path> mklabel gpt)
    if not rootOnly:
        rc = subprocess.run(
        [
            "/usr/bin/parted",      # Bash command for managing partitions
            "--script",             # script flag to remove confirmation / error prompts
            path,                   # this is the drive that you want to check
            "mklabel",              # mklabel = create new partition table
            "gpt"                   # gpt = the type of partition table
        ],
        capture_output=True)
        
        if rc.returncode != 0:
            print('Filed to create partition table on drive ' + path + ' - Return Code: ' + str(rc.returncode))
            print(rc)
            return False
    
    #######################################################
    
    # First partition(s) - EFI + Swap
    if path == primaryDrive:
        rc = create_partition(path, 'EFI', '0%', maxSizeEFI, 'fat32')
        
        #######################################################
        
        # add flags for efi partition
        if rc:
            rc = partition_flag(path, 'boot', 'on')
        else:
            return False
        
        if rc:
            rc = partition_flag(path, 'esp', 'on')
        else:
            return False
        
        if rc:
            rc = partition_flag(path, 'msftdata', 'off')
        else:
            return False

        #######################################################
        
        if rc:
            rc = create_partition(path, 'swap', maxSizeEFI, maxSizeSwap, 'swap')
        else:
            return False
        
        if not rc:
            return False
        
        #######################################################
        
    else:
        # non-primary drives are just set to swap for the first part
        rc = create_partition(path, 'swap', '0%', maxSizeSwap, 'swap')
        
        if not rc:
            return False
    
    #######################################################
    
    # Create System / Root partition
    rc = create_partition(path, 'sysroot', maxSizeSwap, maxSizeRoot, 'btrfs')
    
    if not rc:
            return False

    #######################################################
    
    # Create /home partition
    if not rootOnly:
        rc = create_partition(path, 'home', maxSizeRoot, maxSizeHome, 'btrfs')
        
        if not rc:
                return False
    
    #######################################################

    # Formatting complete
    rc = os.system('/usr/bin/parted --script ' + path + ' print')
    # print(rc)  # 0 = good
    return True


#######################################################
# Builds the RAIDs
#######################################################
def build_raid(path):
    # Parms:
    # path = which RAID to create (sysroot or home)
    
    global raidDrives
    global tempMountDir
    global raidToggle
    
    #######################################################
    
    # Toggles path for later
    pathToggle={
        'sysroot':2,
        'home':3
    }
    
    pathCurrent = pathToggle[path]
    
    #######################################################

    # Checks to see if the tempoary directory exists (returns true/false)
    check = os.path.exists(tempMountDir)
    if check:
        print('Directory ' + tempMountDir + ' already exists, remove it and start job again.')
        return False

    #######################################################

    # Creates tempoary directory (mkdir /btrfsraidmnt)
    rc = subprocess.run(
    [
        "/usr/bin/mkdir",       # Bash command for making directory
        tempMountDir            # Directory to create
    ],
    capture_output=True)
    
    if rc.returncode != 0:
        print('Unable to create directory ' + tempMountDir + ' - Return Code: ' + str(rc.returncode))
        print(rc)
        return False

    #######################################################

    # Mount primary drive for RAID (mount /dev/vda3 /btrfsraidmnt)
    rc = subprocess.run(
    [
        "/usr/bin/mount",                           # Bash command for mounting a drive
        raidDrives[0] + str(pathCurrent + 1),       # Which drive partition to mount
        tempMountDir                                # Directory to assign the mount to
    ],
    capture_output=True)
    
    if rc.returncode != 0:
        print('Unable to mount primary drive for ' + path + ' - Return Code: ' + str(rc.returncode))
        print(rc)
        return False

    #######################################################

    # Merging BtrFS partitions, CMD Command:
    # btrfs device add /dev/vdb1 /dev/vdc1 /dev/vdd1 /dev/vde1 /dev/vdf1 /dev/vdg1 /dev/vdh1 /

    # first part of command
    cmd = [
        '/usr/bin/btrfs',           # Bash command for managing BtrFS partitions
        'device',                   # Manage devices of a BtrFS filesystem
        'add',                      # Add devices defined by the rest of the command
        '-f']                       # Force overwrite of existing filesystem on the given disk(s)

    # add all non-primary drives:
    for i in range(1, len(raidDrives)):
        cmd.append(raidDrives[i] + str(pathCurrent))

    # which mountpoint to add them to:
    cmd.append(tempMountDir)

    # run it
    rc = subprocess.run(cmd ,capture_output=True)
    
    if rc.returncode != 0:
        print('Unable to merge btrfs partitions for ' + path + ' - Return Code: ' + str(rc.returncode))
        print(rc)
        return False
    
    #######################################################

    # Configure RAID + Rebalance the BtrFS drive (btrfs balance start -v -mconvert=raid10 -dconvert=raid10 /)
    rc = subprocess.run(
    [
        '/usr/bin/btrfs',                       # Bash command for managing BtrFS partitions
        'balance',                              # Rebalance the drives based on the new RAID config
        'start',                                # Start the process now
        '-v',                                   # -v = Verbose - Shows all logs / status
        '-f',                                   # -f = Force - there is a warning about metadata redundancy, forcing to remove error
        '-mconvert=raid10',                     # BtrFS in RAID 10 ensures that all drives share the load and there is redundancy
        '-dconvert=' + raidToggle[path],        # RAID config for actual data
        tempMountDir                            # Which mounted drive to run config on
    ],
    capture_output=True)
    
    if rc.returncode != 0:
        print('Filed to create partition table on drive ' + path + ' - Return Code: ' + str(rc.returncode))
        print(rc)
        return False

    #######################################################

    # Display the details of the new BtrFS configuration
    rc = os.system('/usr/bin/btrfs fi usage ' + tempMountDir)
    # print(rc)  # 0 = good
    
    #######################################################

    # Unmount primary drive for RAID (umount /btrfsraidmnt)
    rc = subprocess.run(
    [
        "/usr/bin/umount",              # Bash command for unmounting a drive
        tempMountDir                    # Directory to unmount
    ],
    capture_output=True)
    
    if rc.returncode != 0:
        print('Unable to unmount primary drive for ' + path + ' - Return Code: ' + str(rc.returncode))
        print(rc)
        return False

    # NOTE: Error code 32 = target is busy.
    # This means that you someone currently is using the directory or has a terminal in it

    #######################################################

    # Removes tempoary directory (rm -r /btrfsraidmnt)
    rc = subprocess.run(
    [
        "/usr/bin/rm",       # Bash command for making directory
        "-r",                # r = Recursive (to remove whole directory) 
        tempMountDir         # Directory to remove
    ],
    capture_output=True)
    
    if rc.returncode != 0:
        print('Unable to remove directory ' + tempMountDir + ' - Return Code: ' + str(rc.returncode))
        print(rc)
        return False
    
    #######################################################
    
    # Raid built successfully
    print('Successfully built RAID for: ' + path)
    return True


#######################################################
# Main block of code
#######################################################

# Main menu
print('This script will build a BtrFS RAID for your system:')

print('c = Cancel this run (default)')
print('r = Root, EFI, swap, and @ (system) drives only')
print('f = Full wipe and restore of all drives')

print('[C/r/f]:')
menuChoice = input()

if menuChoice != '':
    menuChoice = str(menuChoice)[0].lower()

selBypass = False

if not selBypass and menuChoice == 'f':
    print('Full build starting...')
    selBypass = True

if not selBypass and menuChoice == 'r':
    print('System root only build starting...')
    selBypass = True
    rootOnly = True

if not selBypass:
    print('Cancelling run.')
    sys.exit(0)

#######################################################

# verifies that the RAID mount directory does not yet exist
check = os.path.exists(tempMountDir)
if check:
    print('ABORT: Directory ' + tempMountDir + ' already exists, remove it and start job again.')
    sys.exit(1)

# turning off swap to prevent a mount error
rc = os.system('swapoff -a')
# print(rc)  # 0 = good

# Performs safety checks on drives
drivesSafe = True
for i in raidDrives:
    status = check_drive(i)
    if status == False:
        drivesSafe = False

if not drivesSafe:
    print('ABORT: Drives did NOT pass safety test, exiting...')
    sys.exit(1)

print('Safety checks passed, formatting drives...')

#######################################################

# Reformats the drives
for i in raidDrives:
    partitionNum = 0
    status = format_drive(i)
    if status == False:
        print('ABORT: Drives did NOT format correctly, exiting...')
        sys.exit(1)

print('Drives formatted, creating RAID...')

#######################################################

sysrootBuilt = build_raid('sysroot')
if not sysrootBuilt:
    print('ABORT: System root RAID did not build exiting...')
    sys.exit(1)

if not rootOnly:
    homeBuilt = build_raid('home')
    if not homeBuilt:
        print('ABORT: Home RAID did not build exiting...')
        sys.exit(1)

print('RAIDs built successfully, proceed with installing your distro')

