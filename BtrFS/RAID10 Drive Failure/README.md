Crafted 1/1/2021

Response to this error:

wrong fs type, bad option, bad superblock on /dev/sdX, 
missing codepage or helper program, or other error.

![Error Message](./images/01-ErrorMessage.png)

## <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

Start by mounting the drive in degraded mode:

```bash
mkdir /tmp/fs/
```
```bash
mount -o degraded /dev/sdb /tmp/fs/
```

open the system directory:

```bash
cd /tmp/fs/@
```

![Mount Degraded](./images/02-MountDegraded.png)

## <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

chroot into that directory

```bash
chroot .
```

![chroot](./images/03-chroot.png)

## <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

You should not be albe to use btrfs CLI:

- NOTE: Do NOT use sudo, it will cause an error:

![chroot](./images/04-NoSudo.png)

```bash
btrfs fi usage /
```

## <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

Delete the failed drive (it will show up as "missing"

- NOTE: This will take some time (it also rebalances your disks)

```bash
btrfs device delete missing /
```

![chroot](./images/05-DeleteFailedDrive.png)

## <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

! - 2nd drive failure caused this to be unbootbable again.  Try this again and see if you need to rebalance manually?

! - this could also be because you had an odd number of disks on RAID10 and needed to rotate a disk out?

## <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

You should now be able to reboot your machine

as long as grub was set up properly before the crash you should be good to go again

## <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

!!! - Revisit this later to see if you can set up GRUB from the terminal with a broken system

## <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<






```bash

```

```bash

```

```bash

```








------------------------------------
